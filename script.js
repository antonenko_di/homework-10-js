document.addEventListener("click", () => {
    const tabs = document.querySelectorAll(".tabs-title");
    const tabsContent = document.querySelectorAll(".content");

    tabs.forEach((tab, index) => {
        tab.addEventListener("click", () => {
            tabs.forEach(el => el.classList.remove("active"));
            tab.classList.add("active");

            tabsContent.forEach(content => {
                content.style.display = "none";
            });
            tabsContent[index].style.display = "block";
        });
    });
});